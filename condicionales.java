// Programación orientada a objetos (Encabezado requerido en todos los programas de POO)
// Condicionales
// By: PCPP

public class condicionales {
	
	public static void main (String[] args){

        	//Mayor qué
		System.out.println(5 > 8); //False
			
		//Menor qué
		System.out.println(4 < 5); //True

		//Mayor igual qué
		System.out.println(5 >= 4); //True

		//Menor igual qué
		System.out.println(5 <= 6); //True

		//Igual Igual
		System.out.println(6 == 7); //False

		//Diferente
		System.out.println(5 != 5); //False

        }
	}